var express=require("express");
var bodyParser=require('body-parser');
var connection = require('./config');
var app = express();
var authenticateController=require('./controllers/authenticate-controller');
var registerController=require('./controllers/register-controller');
var postMsg= require('./controllers/post-controller');
var score= require('./controllers/jeux');
const fs = require('fs');
var path = require('path');

app.use('/views/IMG', express.static(__dirname + "/views/images"));
app.use(express.static(path.join(__dirname, 'views/JS')));
app.use(express.static(path.join(__dirname, 'views/CSS')));
app.use(express.static(path.join(__dirname, 'views/IMG')));
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function (req, res) {
  res.render("login.ejs")
})

//route to handle login and registration
app.post('/api/register',registerController.register);
app.post('/api/authenticate',authenticateController.authenticate);
app.post('/api/posterMessage', postMsg.posterMessage)
app.post('/api/jeux', score.score)

console.log(authenticateController);
app.post('/controllers/register-controller', registerController.register);
app.post('/controllers/authenticate-controller', authenticateController.authenticate);
app.post('/controllers/post-controller', postMsg.posterMessage);
app.post('/controllers/jeux', score.score);


app.get('/', function (req, res) {
  res.render("login.ejs")
})

app.get('/jeu421', function (req, res) {
  res.render('jeu421.ejs')
})

app.get('/nbmyst', function (req, res) {
  res.render('nbmyst.ejs')
})

app.get('/snake', function (req, res) {
  res.render('snake.ejs')
})

app.get('/index', function (req, res) {
  res.render('index.ejs')
})
app.get('/register', function (req, res) {
  res.render('register.ejs')
})

app.get('/compte', function (req, res) {
  res.sendFile(__dirname+ "/controllers/connexions.json")
  //res.render('compte.ejs', {result: contenu} )
})

app.get('/admin', function (req, res) {
  res.render('admin.ejs')
})

app.get('/img/:requested', function(req, res){
  res.sendFile(__dirname + '/views/images/image' + req.params.requested + ".JPG")
})

app.get('/dataCo', function(req, res){
  fs.readFile(__dirname + '/views/JS/connexions.json', {encoding: 'utf8'}, async function (err, data) {
    res.send(data)
  })
})

app.get('/score', function (req, res) {
  var query = connection.query('SELECT * FROM score', function(err, result) {
          var scores=[]
          result.forEach(function(r){
            scores.push(parseInt(r['score']))
          })
          scores.sort()
          console.log(scores)
          res.render('bestScore.ejs',{result: JSON.stringify(result)});
       });
})

app.get('/contactus', function (req, res) {
  var query = connection.query('SELECT * FROM livre', function(err, result) {
          console.log(JSON.stringify(result))
          res.render('contact.ejs',{result: JSON.stringify(result)});
       });
})


app.listen(8080);
