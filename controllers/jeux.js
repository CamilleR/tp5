var connection = require('./../config');
const fs = require('fs');


module.exports.score=function (req, res){
  var score = req.body.resultat;

  var rawdata = fs.readFileSync(__dirname + '/connexions.json')
  var co= JSON.parse(rawdata);
  var name= co['name']
  var sc={
    "name": name,
    "score":score,
    "jeu": ""
  }

  connection.query('INSERT INTO score SET ?',sc, function (error, results, fields) {
    if (error) {
      res.json({
          status:false,
          message:'there are some error with query'
      })
    }else{
      console.log("success to record score")
      res.render("index.ejs")
    }
  });
}
