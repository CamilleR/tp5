var express=require("express");
var connection = require('./../config');
var useragent = require('useragent');

module.exports.register=function(req,res){
    var agent = useragent.parse(req.headers['user-agent']);
    var today = new Date();
    var users={
        "name":req.body.name,
        "email":req.body.email,
        "password":req.body.password,
        "created_at":today,
        "updated_at":today,
        "usr_info": JSON.stringify(agent.toJSON())
    }
    connection.query('INSERT INTO users SET ?',users, function (error, results, fields) {
      if (error) {
        res.json({
            status:false,
            message:'there are some error with query'
        })
      }else{
        console.log("success to register")
        res.render("login.ejs")
      }
    });
    console.log(JSON.stringify(agent.toJSON()))
}
