var nbcoups=1;
var nbMyst;

function generer(){
	var valMax=document.nb.valmax.value;
	nbMyst=Math.round(valMax*Math.random()+1);
	
	document.nb.prop.style.visibility="visible";
	document.getElementById("lblProp").style.display="inline";
	document.nb.test.style.visibility="visible";
	document.nb.prop.focus();
}

function tester(){
	var prop=document.nb.prop.value;
	if(nbcoups===1){
		 document.nb.resultat.style.visibility="visible";
		 document.getElementById("lblR").style.display = "inline"; 
	}
	if (prop==nbMyst){
		document.nb.resultat.value="Gagné en " + nbcoups + " coups";
		document.nb.rejouer.style.visibility="visible";
		document.nb.test.style.visibility="hidden";
	}else{
		nbcoups++;
		if (prop<nbMyst)
			document.nb.resultat.value="C'est plus grand";
		else 
			document.nb.resultat.value="C'est plus petit";        
	}  
	
}

function initialiser(){
	nbcoups=1;
	document.nb.reset();
	document.nb.valmax.focus();

	document.nb.prop.style.visibility="hidden";
	document.nb.resultat.style.visibility="hidden";
	document.nb.test.style.visibility="hidden";
	document.nb.rejouer.style.visibility="hidden"; 
	document.getElementById("lblProp").style.display = "none";
	document.getElementById("lblR").style.display = "none"; 
}