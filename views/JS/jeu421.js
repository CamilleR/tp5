var nbcoups=1;

function generer() {
	var nb=Math.round(5*Math.random()+1);
	return nb;
}

function test() {

	var d1=document.jeu421.de1.value;
	var d2=document.jeu421.de2.value;
	var d3=document.jeu421.de3.value;
	if (((d1==1) || (d2==1) || (d3==1)) && ((d1==2) || (d2==2) || (d3==2)) && ((d1==4) || (d2==4) || (d3==4))) {
		document.jeu421.resultat.value=nbcoups;        
		document.jeu421.resultat.style.visibility="visible";
		document.getElementById("labelR").style.visibility="visible";
		document.getElementById("labelR").style.display = "inline";
		document.jeu421.rejouer.style.visibility="visible";
	}else{
		if (nbcoups===1){
			for (i=1;i<=3;i++){
				document.getElementById("bouton"+i).style.visibility="visible";
			}
		}
		nbcoups++;
		}
}

function initialiser() {
	document.jeu421.reset();
	for (i=1;i<=3;i++)
		document.getElementById("bouton"+i).style.visibility="hidden";
	document.jeu421.resultat.style.visibility="hidden";
	document.getElementById("labelR").style.visibility="hidden";
	document.jeu421.rejouer.style.visibility="hidden";
	nbcoups=1;
}

function first() {
	for (i=1;i<=3;i++){
		document.getElementById("de"+i).value=generer();
		document.getElementById("de"+i).style.visibility="visible" ;
		document.getElementById("label"+i).style.display="inline";
	}
	document.jeu421.resultat.style.visibility="hidden";
	test();
}

function relance(n) {
	document.getElementById("de"+n).value=generer();
	test();
}
