var timer;
var tab;
var image;
var index;

function diaporama(){
	var div = document.getElementById("diaporama");
	tab= div.getElementsByTagName("li");
	index=1;
	image= div.getElementsByTagName('img')[0];
	image.src = tab[0].innerHTML;
	timer=setInterval("suivante()",1000);
}
function suivante(){
	if (index == tab.length )
		index= 0;
	image.src = tab[index].innerHTML;
	index ++;
}

function arreterDiapo(){
	if (timer==null){
		suivante();
		timer= setInterval("suivante()",1000);
		image.title="cliquer sur l'image pour arreter le diaporama"
	}
	else{
		clearInterval(timer);
		timer=null;
		image.title="cliquer sur l'image pour lancer le diaporama";
	}
}

//Fonction permettant de charger la fonction diaporama() et affichage() au chargement de la page.
function charger(){
	diaporama();
}
