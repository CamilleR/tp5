-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le :  jeu. 20 déc. 2018 à 10:29
-- Version du serveur :  5.6.35
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `bddTP5`
--

-- --------------------------------------------------------

--
-- Structure de la table `livre`
--

CREATE TABLE `livre` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `message` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `livre`
--

INSERT INTO `livre` (`id`, `name`, `email`, `phone`, `message`, `created_at`) VALUES
(0, 'jdknl', 'hjbknl,', 'hbjknl', ' jbhknsldazejzbkhesjfekdnzabekhzefa', '2018-12-19 23:34:52'),
(0, 'comile', 'comile@mail.com', '0897902980', 'BONJOUR', '2018-12-19 23:39:18'),
(0, 'Demo', 'demo', '0978679586769708', 'BONOJOURE JE METS UN MESSAGE', '2018-12-19 23:55:36'),
(0, 'Bonjour', 'bonjour', '01982', 'BONJOUR', '2018-12-20 10:01:31'),
(0, 'NBONOJUIYIGYFU', 'GHVJBKHL', 'GVJHBKNL', 'HGVJBKNL?', '2018-12-20 10:02:25');

-- --------------------------------------------------------

--
-- Structure de la table `score`
--

CREATE TABLE `score` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `score` varchar(255) NOT NULL,
  `jeu`varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `score`
--

INSERT INTO `score` (`id`, `name`, `score`) VALUES
(0, 'test', '12',"421"),
(0, 'test', '8',"Nombre mystere"),
(0, 'test', '9', "Nombre mystere"),
(0, 'test', '2', "Nombre mystere"),
(0, 'test', '5', "421");

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `usr_info` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`, `usr_info`) VALUES
(0, 'admin', 'admin@admin.com', 'admin', '2018-12-19 22:01:22', '2018-12-19 22:01:22', '{\"family\":\"Firefox\",\"major\":\"63\",\"minor\":\"0\",\"patch\":\"0\",\"device\":{\"family\":\"Other\",\"major\":\"0\",\"minor\":\"0\",\"patch\":\"0\"},\"os\":{\"family\":\"Mac OS X\",\"major\":\"10\",\"minor\":\"14\",\"patch\":\"0\"}}'),
(0, 'test', 'test@admin.com', 'test', '2018-12-19 22:01:53', '2018-12-19 22:01:53', '{\"family\":\"Firefox\",\"major\":\"63\",\"minor\":\"0\",\"patch\":\"0\",\"device\":{\"family\":\"Other\",\"major\":\"0\",\"minor\":\"0\",\"patch\":\"0\"},\"os\":{\"family\":\"Mac OS X\",\"major\":\"10\",\"minor\":\"14\",\"patch\":\"0\"}}');
