var mysql      = require('mysql');
var connection = mysql.createConnection({
  "host": "127.0.0.1",
  "port": 3306,
  "database": "bddTP5",
  "password": "root",
  "name": "db",
  "user": "root",
  "connector": "mysql",
  "socketPath": "/Applications/MAMP/tmp/mysql/mysql.sock"
});
connection.connect(function(err){
if(!err) {
    console.log("Database is connected");
} else {
    console.log("Error while connecting with database");
}
});
module.exports = connection;
